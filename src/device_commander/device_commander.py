from os import environ
from collections import defaultdict
from pyjung import JungRegistry


class DeviceCommander(JungRegistry):
    """ Route commands to proper device gateways.

    The device commander microservice is responsible for routing commands from the API
    gateway to the target device through the proper device gateway.
    """

    def __init__(self, kafka_broker, in_topic, out_topic, group, port='9092'):

        super().__init__(kafka_broker, in_topic, out_topic, port, group)

    def process_message(self, message):
        task_id = message["id"]
        task = message["task"]
        device_id = message["content"]["device_id"]

        response = {"task_id": task_id, "task": task}
        result = None

        if task == "COMMAND":
            if device_id not in self.storage:
                self.tasker.set_topics(in_topic="device_results",
                                   out_topic="device_tasks")
                device_req = self.tasker.create_task("GET", {"device_id": device_id})
                self.tasker.publish(device_req)
                
                device = self.tasker.get_task_result(device_req["id"])
                self.storage[device_id] = device["gateway"]

            gateway = self.storage[device_id]
            self.tasker.set_topics(in_topic=gateway + "_results",
                                   out_topic=gateway + "_tasks")
            
            self.tasker.publish(message)
            result = self.tasker.get_task_result(task_id)
            
        self.tasker.reset_topics()
        response["result"] = result
        return response


def main():
    kafka_broker = environ["KAFKA_BROKER"]
    device_commander = DeviceCommander(kafka_broker=kafka_broker,
                                       in_topic="command_tasks",
                                       out_topic="command_results",
                                       port="9092",
                                       group="device-commander")
    device_commander.process_tasks()


if __name__ == "__main__":
    main()
