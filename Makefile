NAME:=device-commander
SRC_DIR:=src
PACKAGE_NAME:=device_commander


module: ROOTFS usr.manifest

ROOTFS/tmp:
	mkdir -p ROOTFS/tmp/$(NAME)
	touch ROOTFS/tmp

ROOTFS/$(NAME):
	mkdir -p ROOTFS
	cp -r $(SRC_DIR)/$(PACKAGE_NAME) ROOTFS/$(NAME)
	touch ROOTFS/$(NAME)


usr.manifest:
	echo "/$(NAME)/**: \$${MODULE_DIR}/ROOTFS/$(NAME)/**" > usr.manifest

clean:
	rm -rf ROOTFS $(DIR)
	rm -f usr.manifest

ROOTFS: ROOTFS/$(NAME) ROOTFS/tmp
