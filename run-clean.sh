#!/bin/bash

DEFAULT_KAFKA="192.168.0.100"
ARG1=${1:-KAFKA_BROKER=$DEFAULT_KAFKA}

sudo make clean && sudo capstan rmi device-commander && sudo capstan run -e '--env='$ARG1' /python.so /device-commander/device_commander.py' -n bridge
